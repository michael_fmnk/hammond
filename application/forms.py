from flask_wtf import FlaskForm
from wtforms import (DateField, HiddenField, PasswordField, SelectField,
                     StringField, TextAreaField)
from wtforms.fields import FileField, IntegerField
from wtforms.fields.html5 import DecimalRangeField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])


class OrderForm(FlaskForm):
    firstname = StringField("Firsname", validators=[DataRequired()])
    lastname = StringField("Lastname", validators=[DataRequired()])
    email = StringField("Email", validators=[DataRequired()])
    date = DateField("date", format='%d/%m/%Y', validators=[DataRequired()])
    comment = TextAreaField("comment", validators=[DataRequired()])
    id = HiddenField()


class PostForm(FlaskForm):
    title = StringField("title", validators=[DataRequired()])
    text = TextAreaField("text", validators=[DataRequired()])


class AddProductForm(FlaskForm):
    title = StringField("title", validators=[DataRequired()])
    price = IntegerField("price", validators=[DataRequired()])
    area = IntegerField("area", validators=[DataRequired()])
    product_margin = DecimalRangeField()
    type_of_product = SelectField("type", choices=[("house", "House"),
                                                   ("flat", "Flat"),
                                                   ("penthouse", "Penthouse")])
    description = TextAreaField("description", validators=[DataRequired()])
    image1 = FileField()
    image2 = FileField()
    id = HiddenField()

    def set_values(self, item):
        self.area.data = item.get('area', 0)
        self.title.data  = item.get('title','')
        self.price.data = item.get('price',0)
        self.product_margin.data = item.get('profit', 0)*100
        self.description.data = item.get("description", '')
        self.id.data = item['_id']
