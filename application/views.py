import json
import sys  # debug
from datetime import *
from math import ceil

import pymongo  # todelete
from bson.objectid import ObjectId
from flask import (flash, jsonify, redirect, render_template, request, session,
                   url_for)
from flask_login import login_required, login_user, logout_user

from application import app
from config import *

from .forms import *
from .models import Post, Product, User as user_service
from .user import User
from .user_loader import *


@app.route("/")
def homepage():
    form = LoginForm()
    return render_template("homepage.html", form=form)


@app.route("/products")
def products_render():
    products = Product.objects.all()
    print(products, file=sys.stderr)
    return render_template("productspage.html", products=products,
                           new=request.args.get("new"))


@app.route("/products/<id>")
def product_info(id):
    simmilar = Product.objects.all()
    item = Product.objects.get(_id=id)
    return render_template('productinfo.html', products=simmilar, item=item)


@app.route("/login", methods=["POST", "GET"])
def login():
    form = LoginForm()
    if request.method == "POST" and form.validate_on_submit():
        user = user_service.objects.get(username=form.username.data)
        if user and User.validate_login(user.password, form.password.data):
            user_obj = User(user.username)
            login_user(user_obj)
            flash("Logged in successfully", category="success")
            return redirect(request.args.get("next") or url_for("admin.adminpage"))
        flash("Wrong username or password", category="error")
    return redirect(url_for("homepage"))


@app.route("/order/<id>")
def order(id):
    form = OrderForm()
    product = products_collection.find_one({"_id": ObjectId(id)})
    return render_template("orderpage.html", product=product, form=form)


@app.route("/process_order", methods=["POST", "GET"])
def process_order():
    form = OrderForm()
    if form.validate_on_submit():
        firstname = form.firstname.data
        lastname = form.lastname.data
        email = form.email.data
        date = form.date.data
        building_id = form.id.data
        comment = form.comment.data
        orders_collection.insert_one({
            "firstname": firstname,
            "lastname": lastname,
            "email": email,
            "start_date": datetime.combine(form.date.data,
                                           datetime.min.time()),
            "building_id": ObjectId(building_id),
            "comment": comment
        })
    return render_template("completedorder.html", id=1)


@app.route('/checkout', methods=["GET", "POST"])
def checkout():
    form = OrderForm()
    if request.method == "POST":
        firstname = form.firstname.data
        lastname = form.lastname.data
        email = form.email.data
        date = form.date.data
        building_id = form.id.data
        comment = form.comment.data
        orders_collection.insert_one({
            "firstname": firstname,
            "lastname": lastname,
            "email": email,
            "start_date": datetime.combine(form.date.data,
                                           datetime.min.time()),
            "building_id": list(map(lambda x: ObjectId(x), session['cart'])),
            "comment": comment
        })
        return redirect(url_for("homepage"))

    if "cart" not in session:
        return redirect(url_for("homepage"))
    ids = list(map(lambda x: ObjectId(x), session['cart']))
    cart = list(products_collection.find({
        "_id": {
            "$in": ids
        }
    }))
    sum = 0
    for i in cart:
        sum += i['price']
    return render_template('checkoutpage.html', cart=cart, form=form)


@app.route("/blog")
def newsfeed():
    news = Post.objects.all()
    count = ceil(news.count()/10)
    news = news.limit(10)
    return render_template('newsfeedpage.html', news=news, pages=count, page=1)


@app.route("/blog/<int:id>")
def newsfeedid(id):
    pages = ceil(news.count()/10)
    if id > pages:
        return redirect(url_for("newsfeed"))
    news = news.skip((id-1)*10).limit(10)
    return render_template('newsfeedpage.html', news=news, pages=pages, page=id)


# REST_API

@app.route("/abc")
def pr():
    allof = list(products_collection.find({}))
    for i in allof:
        i["_id"] = str(i['_id'])
    response = app.response_class(
        response=json.dumps(allof),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route("/send", methods=["POST"])
def process_email():
    email = request.form["email"]
    date = datetime.now()
    subscribers_collection.insert_one({"email": email, "date": date})
    return json.dumps({"status": "OK"})


@app.route("/products/<id>/add_to_list", methods=["POST", "GET"])
def add_product_to_shopping_list(id):
    if "cart" not in session:
        session["cart"] = []
    session['cart'].append(id)
    session.modified = True
    print(session['cart'], file=sys.stderr)
    return json.dumps({"status": "OK"})


@app.route('/inlist')
def inlist():
    if "cart" not in session:
        return jsonify("")
    ids = list(map(lambda x: ObjectId(x), session['cart']))
    cart = list(products_collection.find({
        "_id": {
            "$in": ids
        }
    }))
    for i in cart:
        i["_id"] = str(i['_id'])
    response = app.response_class(
        response=json.dumps(cart),
        status=200,
        mimetype='application/json'
    )
    return response
