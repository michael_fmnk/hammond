$(function(){
    $("#flats").click(function(){
        $(".container").css("display", "inline-block");
        $(".container").not($(".flat")).css("display", "none");
    })
    $("#houses").click(function(){
        $(".container").css("display", "inline-block");
        $(".container").not($(".house")).css("display", "none");
    })
    $("#penthouses").click(function(){
        $(".container").css("display", "inline-block");
        $(".container").not($(".penthouse")).css("display", "none");
    })
    $("#all").click(function(){
        $(".container").css("display", "inline-block");
    })

})