from mongoengine import (Document, IntField, ListField, ObjectIdField,
                         StringField)

from application import db


class Post(db.Document):
    title = StringField(max_length=200, required=True)
    meta = {'collection': 'posts', 'strict': False}


class User(db.Document):
    username = StringField()
    password = StringField()
    meta = {'collection': 'admins', 'strict': False}


class Product(db.Document):
    _id = ObjectIdField()
    area = IntField()
    description = StringField()
    title = StringField()
    price = IntField()
    image = ListField()
    meta = {'collection': 'products', 'strict': False}
