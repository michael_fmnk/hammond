from hashlib import sha256


class User():

    def __init__(self, username):
        self.username = username

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.username

    @staticmethod
    def validate_login(password_hash, password):
        hashed = sha256(password.encode("utf-8")).hexdigest()
        if hashed == password_hash:
            return True
        else:
            return False
