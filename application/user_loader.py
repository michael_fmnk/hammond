from application import lm
from .models import User as user_service
from .user import User


@lm.user_loader
def load_user(username):
    user = user_service.objects.get(username=username)
    if not user:
        return None
    return User(user.username)