from flask import Flask
from flask_cors import CORS
from flask_wtf.csrf import CSRFProtect
from flask_login import LoginManager
from .admin.views import admin
from flask.ext.mongoengine import MongoEngine
from mongoengine import StringField
UPLOAD_FOLDER = "application/static/images/products/"
app = Flask(__name__)
CORS(app)
app.secret_key = "5accdb11b2c10a78d7c92c5fa102ea77fcd50c2058b00f6e"
app.config['DEBUG'] = True
app.config['MONGODB_SETTINGS'] = {
    'DB': 'buildme'
}
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.register_blueprint(admin)
csrf = CSRFProtect(app)
db = MongoEngine(app)


class Admin(db.Document):
    username = StringField(max_length=200, required=True)
    password = StringField(max_length=200, required=True)
    meta = {'collection': 'admins'}


csrf.init_app(app)
lm = LoginManager()
lm.init_app(app)
lm.login_view = "login"



from application import views