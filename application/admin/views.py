from flask_login import login_required, current_user, logout_user
from config import *
from bson.objectid import ObjectId
import pymongo
from application.forms import *
from flask import render_template, request, redirect, url_for, Blueprint

admin = Blueprint("admin", __name__, template_folder='../templates/admin/')


@admin.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("homepage"))


@admin.route("/admin", methods=["GET"])
@login_required
def adminpage():
    orders = list(orders_collection.aggregate([{
        "$lookup": {
            "from": "products",
            "localField": "building_id",
            "foreignField": "_id",
            "as": "product"
        }
    }]))
    revenue = 0
    total_profit = 0
    for order in orders:
        for building in order['product']:
            revenue += building['price']
            total_profit += building['profit'] * building['price']

    subs = subscribers_collection.find({}).sort("date", pymongo.DESCENDING)
    return render_template("adminpage.html", subs=subs, user=current_user,
                           orders=orders, revenue=revenue, profit=total_profit)


@admin.route('/products/<id>/edit', methods=["GET", "POST"])
@login_required
def edit(id):
    form = AddProductForm()
    if request.method == "POST":
        products_collection.update_one(
            {
                "_id": ObjectId(id)
            },
            {
                "$set": {
                    "title": form.title.data,
                    "description": form.description.data,
                    "area": form.area.data,
                    "price": form.price.data,
                    "profit": float(form.product_margin.data / 100)
                }
            }
        )

        return redirect(url_for("products_render", new=True))
    item = products_collection.find_one({"_id": ObjectId(id)})
    form.set_values(item)
    return render_template("editproductpage.html", form=form, id=id)


@admin.route("/products/<id>/delete")
@login_required
def delete_product(id):
    products_collection.remove({"_id": ObjectId(id)})
    return redirect(url_for("products_render"))


@admin.route('/admin/add', methods=["GET", "POST"])
@login_required
def add_product():
    form = AddProductForm()
    if request.method == "POST":
        img1 = form.image1.data
        img2 = form.image2.data
        img1_filename = secure_filename(img1.filename)
        img2_filename = secure_filename(img2.filename)
        img1.save(app.config['UPLOAD_FOLDER'] + img1_filename)
        img2.save(app.config['UPLOAD_FOLDER'] + img2_filename)
        real_margin = float(form.product_margin.data / 100)
        products_collection.insert_one({
            "title": form.title.data,
            "price": form.price.data,
            "type": form.type_of_product.data,
            "description": form.description.data,
            "profit": real_margin,
            "image": [img1_filename, img2_filename]
        })
        return redirect(url_for("products_render", new="true"))
    return render_template("addproductpage.html", form=form)


@admin.route("/admin/addpost")
@login_required
def add_post():
    form = PostForm()
    return render_template("addpostpage.html", form=form)